﻿
namespace Lands.Domain
{
    using System.Data.Entity;

    public class DataContext : DbContext
    {
        // hereda  del superconstructor de la clase origen
        public DataContext() : base("DefaultConnection")
        {

        }

        public System.Data.Entity.DbSet<Lands.Domain.User> Users { get; set; }

        public System.Data.Entity.DbSet<Lands.Domain.UserType> UserTypes { get; set; }
    }
}
