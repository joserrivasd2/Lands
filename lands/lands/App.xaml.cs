﻿
namespace Lands
{
    using Xamarin.Forms;
    using Views;
    using Helpers;
    using ViewModels;
    using Lands.Services;
    using Lands.Models;

    public partial class App : Application
	{
        #region Property
        public static NavigationPage Navigator { get; internal set; } 
        #endregion
        #region Constructors
        public App ()
		{
			InitializeComponent();

            if (string.IsNullOrEmpty(Settings.Token)) // si el token esta vacio
            {
                this.MainPage = new NavigationPage(new LoginPage());
            }
            else
            {
                var dataService = new DataService();
                // primer registro de la tabla User local.. false(sin children)
                var user = dataService.First<UserLocal>(false); 
                var mainViewModel = MainViewModel.GetInstance();
                mainViewModel.User = user;
                mainViewModel.Token = Settings.Token;
                mainViewModel.TokenType = Settings.TokenType;
                
                mainViewModel.Lands = new LandsViewModel();
                this.MainPage = new MasterPage();
            }

            //this.MainPage = new LoginPage();
            //this.MainPage = new NavigationPage(new LoginPage());
            //this.MainPage = new MasterPage();
        }
        #endregion
        #region Methods

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
        #endregion
    }
}
