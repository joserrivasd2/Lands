﻿

namespace Lands.ViewModels
{
    using Lands.Models;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Lands.Services;
    using Xamarin.Forms;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using System.Linq;

    public class LandsViewModel : BaseViewModel // debe de refrescar la lista de paises
    {
        //INSTANCIANDO EL SERVICIO
        #region Servicios
        private ApiService apiService;
        #endregion

        #region Atributos // MINUSCULAS
        private ObservableCollection<LandItemViewModel> lands; // observableColleccion porq lo vamos a pintar en un list view
        private bool isRefreshing; // isRefreshing activiti indicador propio de list view
        private string filter;
        //private List<Land> landsList; // declarando la variable como atributo de clase para q conserve la info del servicio
        #endregion

        #region Propiedades // MAYUSCULAS
        public ObservableCollection<LandItemViewModel> Lands
        {
            get { return this.lands; }
            set { SetValue(ref this.lands, value); }
        }
        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }
        public string Filter
        {
            get { return this.filter; }
            set {
                SetValue(ref this.filter, value);
                // para realizar busqueda automatica despues q actualice el valor realice la busqueda
                this.Search();
            }
        }
        #endregion

        #region Constructores
        public LandsViewModel()
        {
            this.apiService = new ApiService(); // los servicios se deben instanciar en el constructor
            this.LoadLands();
        }

        #endregion

        #region Metodos
        private async void LoadLands()
        {
            this.IsRefreshing = true; // propiedad activity indicator
            // VERIFICANDO CONEXION siempre validar en cada page
            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                   "Error",
                       connection.Message,
                   "Accept");
                // pop modal desaplilando de el objeto navigation(regresando a la pag login) (push para apilar pop para desapilar)
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }
            var apiLands = Application.Current.Resources["APILands"].ToString();
            var response = await this.apiService.GetList<Land>
                (
                //"h ttp://restcountries.eu",
                apiLands,
                "/rest",
                "/v2/all");
            // toda la direccion http://restcountries.eu/rest/v2/all
            if (!response.IsSuccess) //true si el serrvicio devuelve valores
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error", 
                        response.Message,
                    "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }
            // CONVIRTIENDO LISTA A OBSERVABLE COLLECTION

            //var list = (List<Land>)response.Result; // declarando la list como variable local
            //this.landsList = (List<Land>)response.Result; // asignando valor list a atributo de clase para facilitar la persistencia de los datos

            MainViewModel.GetInstance().LandsList = (List<Land>)response.Result;
            this.Lands = new ObservableCollection<LandItemViewModel>(this.ToLandItemViewModel());
            this.IsRefreshing = false;
        }

        #endregion
        #region MyRegion
        private IEnumerable<LandItemViewModel> ToLandItemViewModel()
        {
            // return this.landsList.Select(l => new LandItemViewModel
            return MainViewModel.GetInstance().LandsList .Select(l => new LandItemViewModel
            {
                Alpha2Code = l.Alpha2Code,
                Alpha3Code = l.Alpha3Code,
                AltSpellings = l.AltSpellings,
                Area = l.Area,
                Borders = l.Borders,
                CallingCodes = l.CallingCodes,
                Capital = l.Capital,
                Cioc = l.Cioc,
                Currencies = l.Currencies,
                Demonym = l.Demonym,
                Flag = l.Flag,
                Gini = l.Gini,
                Languages = l.Languages,
                Latlng = l.Latlng,
                Name = l.Name,
                NativeName = l.NativeName,
                NumericCode = l.NumericCode,
                Population = l.Population,
                Region = l.Region,
                RegionalBlocs = l.RegionalBlocs,
                Subregion = l.Subregion,
                Timezones = l.Timezones,
                TopLevelDomain = l.TopLevelDomain,
                Translations = l.Translations,
            });
        }
        #endregion

        #region Commands
        public ICommand RefreshCommand 
        {
            // un comando es una propiedad de solo lectura
            get
            {
                return new RelayCommand(LoadLands);
            }
        }

        public ICommand SearchCommand {
            get
            {
                return new RelayCommand(Search);
            }
        }

        private void Search()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {
                // si el filtro esta vacio cargue lista original
                this.Lands = new ObservableCollection<LandItemViewModel>(this.ToLandItemViewModel());
            }
            else
            {
                this.Lands = new ObservableCollection<LandItemViewModel>(
                    this.ToLandItemViewModel().Where(
                        l => l.Name.ToLower().Contains(this.Filter.ToLower()) ||
                        l.Capital.ToLower().Contains(this.Filter.ToLower())
                        )
                    );
            }
        }
        #endregion
    }
}
