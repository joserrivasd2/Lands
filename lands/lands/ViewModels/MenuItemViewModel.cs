﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;

namespace Lands.ViewModels
{
    using System.Text;
    using System.Windows.Input;
    using Xamarin.Forms;
    using Views;
    using Helpers;
    public class MenuItemViewModel
    {
        #region Properties
        public string Icon { get; set; }

        public string Title { get; set; }

        public string PageName { get; set; }

        #endregion
        #region Comandos

        public ICommand NavigateCommand {
            get
            {
                return new RelayCommand(Navigate);
            }
            
        }

        private void Navigate()
        {
            if (this.PageName == "LoginPage")
            {
                Settings.Token = string.Empty;
                Settings.TokenType = string.Empty;
                var mainViewModel = MainViewModel.GetInstance();
                mainViewModel.Token = string.Empty;
                mainViewModel.TokenType = string.Empty;

                Application.Current.MainPage = new NavigationPage(new LoginPage());
                //quitando de persistencias
                
            }
        }



        #endregion

    }
}
