﻿namespace Lands.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using System.Windows.Input;
    using Xamarin.Forms;
    using Views;
    using Lands.Services;
    using Helpers;
    using System;

    public class LoginViewModel : BaseViewModel // implementando interfaz NotifyPropertyChanged para que me permita hacer un refresco o redenderizacion
    {
        // instanciando servicio creado y almacenado en azure
        #region Servicios 
        private ApiService apiService;
        private DataService dataService;
        #endregion
        //una propiedad privada para cada elemento a refrescar
        #region Attributes
        private string email;
        private string password;
        private bool isRunning;
        private bool isEnabled;

        #endregion


        #region Properties
        public string Email {
            get { return this.email; }
            set { SetValue(ref this.email, value); }
        }
        public string Password {
            get {return this.password;}
            set{ SetValue(ref this.password, value); }
        }
        public bool IsRunning
        {  // ActivityIndicator
            get { return this.isRunning; }
            set { SetValue(ref this.isRunning, value); }
        }
        public bool IsRemember { get; set; } // elemento switch de loginPage
        public bool IsEnabled { // habilitar botones
            get { return this.isEnabled; }
            set { SetValue(ref this.isEnabled, value); }
        } 
        #endregion

        #region Constructor
        public LoginViewModel() // el el constructor puedo manipular propiedades sin refrescar
        {
            this.apiService = new ApiService();
            this.dataService = new DataService();

            this.IsRemember = true;
            this.IsEnabled = true; // propiedad Enable para botones login

            // http://restcountries.eu/rest/v2/all
        }

        #endregion

        #region Commands
        public ICommand LoginCommand {
            get
            {
                return new RelayCommand(Login); 
            }

        }

        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.EmailValidator,
                    Languages.Accept);
                return;
            }
            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You mask enter an Password.",
                    "Accept");
                return;
            }

            this.IsRunning = true;
            this.IsEnabled = false;

            //if (this.Email != "jose@gmail" || this.Password != "1234")
            //{
            //    this.IsRunning = false;
            //    this.IsEnabled = true;
            //    await Application.Current.MainPage.DisplayAlert(
            //       "Error",
            //       "Email or password incorrect",
            //       "Accept");
            //    this.Password = string.Empty;
            //    return;
            //}

            //validando coleccion
            var connection = await this.apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Accept");
                return;
            }
            // creando token
            var apiSecurity = Application.Current.Resources["APISecurity"].ToString();
            var token = await this.apiService.GetToken(
                apiSecurity,
            this.Email, 
                this.Password); //todos los elementos del api son asincronos
            if (token == null)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                   "Error",
                   "Sommething was wrong, Please try later",
                   "Accept");
                return;
                
            }

            if (string.IsNullOrEmpty(token.AccessToken))
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                   "Error",
                   token.ErrorDescription,
                   "Accept");
                this.Password = string.Empty;
                return;
            }

            var user = await this.apiService.GetUserByEmail(
                apiSecurity,
                "/api",
                "/Users/GetUserByEmail",
                this.Email
                );

            var userLocal = Converter.ToUserLocal(user);

            var mainViewModel = MainViewModel.GetInstance();
            // lo mandamos a la mainViewModel para tenerlo en memoria
            mainViewModel.Token = token.AccessToken;
            mainViewModel.TokenType = token.TokenType;
            mainViewModel.User = userLocal;


            if (this.IsRemember)
            {
                // guardandolo en persistencia
                Settings.Token = token.AccessToken;
                Settings.TokenType = token.TokenType;
                //borramos todo y insertamos el ultomo usuario
                this.dataService.DeleteAllAndInsert(userLocal);
            }
            

            mainViewModel.Lands = new LandsViewModel(); // garantizamos q antes de pintar la LancePage establecemos la LanceViewModel Alineada a la Misma

            //await Application.Current.MainPage.Navigation.PushAsync(new LandsPage());
            Application.Current.MainPage = new MasterPage();


            this.IsRunning = false;
            this.IsEnabled = true;

            //Reiniciando password e email para q cuando regrese no esten ahi
            this.Email = string.Empty;
            this.Password = string.Empty;

         
        }


        public ICommand RegisterCommand {
            get
            {
                return new RelayCommand(Register);
            }
        }

        private async void Register()
        {
            // antes de instanciar pagina instancio la view model
            MainViewModel.GetInstance().Register = new RegisterViewModel(); 
            await Application.Current.MainPage.Navigation.PushAsync(new RegisterPage());
        }
        #endregion
    }
}
