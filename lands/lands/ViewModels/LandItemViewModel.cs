﻿
namespace Lands.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Lands.Models;
    using Lands.Views;
    using System;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class LandItemViewModel: Land
    {
        #region Commands
        public ICommand SelectLandCommand {
            get {
                return new RelayCommand(SelectLand);
            }
        }

        private async void SelectLand() // OJO EL METODO ES ASYNC PORQ EL METO DE NAVEGACION ES ASINCRONO
        {
            MainViewModel.GetInstance().Land = new LandViewModel(this);
            //await Application.Current.MainPage.Navigation.PushAsync(new LandTabbedPage());
            await App.Navigator.PushAsync(new LandTabbedPage());
        }
        #endregion
    }
}
