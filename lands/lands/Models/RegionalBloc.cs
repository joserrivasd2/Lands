﻿namespace Lands.Models
{
    using Newtonsoft.Json;
    public class RegionalBloc
    {
        [JsonProperty(PropertyName = "acronym")] // nombre como lo recibo
        public string Acronym { get; set; } // nombre como lo voy a manejar
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
    }
}
