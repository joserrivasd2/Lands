﻿namespace Lands.Models
{
    using Newtonsoft.Json;
   public class Language
    {
        [JsonProperty(PropertyName = "iso639_1")] // nombre como lo recibo
        public string Iso6391 { get; set; } // nombre como lo voy a manejar
        [JsonProperty(PropertyName = "iso639_2")]
        public string Iso6392 { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "nativeName")]
        public string NativeName { get; set; }
    }
}
