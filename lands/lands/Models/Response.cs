﻿namespace Lands.Models
{
    // servicios para consumir api 
    public class Response
    {
        public bool IsSuccess  // cuando solicito un servicio puede o no puede extraer datos
        {
            get;
            set;
        }
        public string Message // si isSucces viene false en esta propiedad vendra la descripcion del error
        {
            get;
            set;
        }
        public object Result //devolvera la lista de paises en el caso del servicio utilizado en el ejemplo
        {
            get;
            set;
        }
    }
}
