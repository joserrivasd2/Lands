﻿

namespace Lands.Models
{
    using Newtonsoft.Json;
    public class Currency
    {
       // EN C# LAS PROPIEDADES COMIENZAN CON MAYUSCULA COMO STANDARD
        [JsonProperty(PropertyName="code")] //nombre como la resivo
        public string Code { get; set; }// nombre con el que la voy a manejar
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "symbol")]
        public string Symbol { get; set; }
    }
}
